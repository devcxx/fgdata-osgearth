McDonnell Douglas DC-10-30
Alpha release for FG 2.4.0, v0.0.2

This README is incomplete.

Special thanks to:
------------------

 * Ampere Hardraade for the internal GE CF6C-50 jet engine model.

 * Syd Adams for the electrical system, the HSI, and for just being one of the best aircraft developers in FlightGear. :)

 * Heiko Schulz for the altimeter from the 737-300.

 * Csaba Halasz ("Jester") and Anders Gidenstam ("AndersG") for assistance and various pointers over FlightGear IRC.

 * Thomas Ito-Haigh ("Armchair Ace") for the Northwest Airlines livery, the Japan Air Systems livery, the Canadien International livery, and the engine fandisk texture.

 * The (many) authors of the KC-135E for making air-to-air refueling possible in FlightGear, and for the tanker boom model which I heavily adapted for the KC-10A.

 * The awesome folks at Atlas Virtual Airlines for supporting me during this project!

...And of course, to the many wonderful developers of FlightGear Flight Simulator! Enjoy your DC-10; happy flying!
