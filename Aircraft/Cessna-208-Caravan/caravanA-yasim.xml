<?xml version="1.0" encoding="UTF-8"?>

<!-- Cessna 208 Caravan - 2010 Emmanuel BARANGER
   
     sources          : http://en.wikipedia.org/wiki/Cessna_208

     wingspan         :    52 ft 1 in      ( 15.88 m    )
     length           :    41 ft 7 in      ( 12.67 m    )
     height           :    14 ft 2 in      (  4.32 m    )
     empty weight     :  4570 lb           (  2078 kg   )
     engine           : PT6A-114 turboprop (   675 hp   )
     Cruise speed     :  184 kts           (   340 km/h )
-->

<airplane mass="4570">

  <!-- Approach configuration -->
  <approach speed="70" aoa="6" fuel="0.2">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="0.2"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="1.0"/>
    <control-setting axis="/controls/flight/flaps" value="1.0"/>
  </approach>

  <!-- Cruise configuration -->
  <cruise speed="184" alt="6000" fuel="0.5">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="0.7"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="0.6"/>
    <control-setting axis="/controls/flight/flaps" value="0.0"/>
  </cruise>

  <cockpit x="1.355" y="0.323" z="0.080"/>

  <fuselage ax="5.787" ay="0" az="0.433" bx="-6.211" by="0" bz="0.701" width="1.760" taper="0.5" midpoint="0.25"/>

  <wing x="1.135" y="0.609" z="1.129" 
        chord="2.165" 
        length="7.296" 
        taper="0.6"
        sweep="-1"
        dihedral="3"
        camber="0.047">
        <stall aoa="24" width="1" peak="1.5"/>
        <flap0 start="0.045" end="0.716" lift="1.2" drag="1.25"/>
        <flap1 start="0.716" end="0.984" lift="1.2" drag="1.1"/>

        <control-input axis="/controls/flight/flaps" control="FLAP0"/>
        <control-input axis="/controls/flight/aileron" control="FLAP1" split="true"/>
        <control-input axis="/controls/flight/aileron-trim" control="FLAP1" split="true"/>

        <control-output control="FLAP0" prop="/surface-positions/flap-pos-norm"/>
        <control-output control="FLAP1" side="left" prop="/surface-positions/left-aileron-pos-norm"/>
        <control-output control="FLAP1" side="right" prop="/surface-positions/right-aileron-pos-norm"/>

        <control-speed control="FLAP0" transition-time="2"/>
  </wing>

  <hstab x="-5.231" y="0" z="0.763" 
         chord="1.263" 
         length="3.120"
         sweep="3"
         taper="0.7">
         <stall aoa="22" width="1" peak="1.3"/>
         <flap0 start="0.10" end="1" lift="1.4" drag="1.1"/>

         <control-input axis="/controls/flight/elevator" control="FLAP0"/>
         <control-input axis="/controls/flight/elevator-trim" control="FLAP0"/>

         <control-output control="FLAP0" prop="/surface-positions/elevator-pos-norm"/>
  </hstab>

  <vstab x="-4.970" y="0" z="0.849" 
         chord="2.5"
         length="2.5"
         taper="0.32"
         sweep="22">
         <stall aoa="14" width="1" peak="1.3"/>
         <flap0 start="0" end="1" lift="1.4" drag="1.35"/>

         <control-input axis="/controls/flight/rudder" control="FLAP0" invert="true"/>
         <control-input axis="/controls/flight/rudder-trim" control="FLAP0" invert="true"/>

         <control-output control="FLAP0" prop="/surface-positions/rudder-pos-norm" min="1" max="-1"/>
  </vstab>

  <!-- Engines   source            : http://en.wikipedia.org/wiki/Pratt_%26_Whitney_Canada_PT6

                 name              : Pratt & Whitney Canada PT6
                 type              : Turboprop
                 power cruise      : 675 hp
                 weight            : 346 lb
  -->
  <propeller x="4.944" y="0" z="0.433" 
             radius="1.475"
	     mass="346" 
	     moment="6"  
	     cruise-speed="184" 
	     cruise-rpm="2500"
	     cruise-alt="6000" 
	     cruise-power="640"
	     takeoff-power="675" 
	     takeoff-rpm="2700">
             <actionpt x="5.912" y="0" z="0.433"/>
             <turbine-engine eng-rpm="2700"
                             eng-power="675"
                             alt="6000" 
                             flat-rating="2200" 
                             min-n2="70" 
                             max-n2="100">
                             <control-input control="THROTTLE" axis="/controls/engines/engine[0]/throttle"/>
                             <control-input control="STARTER" axis="/controls/engines/engine[0]/starter" />
                             <control-input control="MAGNETOS" axis="/controls/engines/engine[0]/magnetos"/>
                             <control-input control="MIXTURE" axis="/controls/engines/engine[0]/mixture"/>
             </turbine-engine>
  </propeller>
  
  <!-- Retractable wheels 0/1/2/3 -->
  <gear x="6.024" y="1.567" z="-2.783" 
        compression="0.2"  
        spring="2" 
        sfric="0.5" 
        dfric=".45" 
        retract-time="4" 
        initial-load="0" 
        damp="0.6">
        <control-input control="STEER" axis="/controls/flight/rudder" square="true"/>
        <control-input axis="/controls/gear/gear-down" control="EXTEND"/>
        <control-output control="EXTEND" prop="/gear/gear[0]/position-norm"/>
        <control-speed control="EXTEND" transition-time="4"/>
  </gear>

  <gear x="1.242" y="1.567" z="-2.718" 
        compression="0.2" 
        spring="3.5" 
        sfric="0.5" 
        dfric=".45" 
        retract-time="4" 
        initial-load="1" 
        damp="10.1">
        <control-input control="BRAKE" axis="/controls/gear/brake-parking" />
        <control-input control="BRAKE" axis="/controls/gear/brake-left" />
        <control-input axis="/controls/gear/gear-down" control="EXTEND"/>
        <control-output control="EXTEND" prop="/gear/gear[1]/position-norm"/>
        <control-speed control="EXTEND" transition-time="4"/>
  </gear>

  <gear x="6.024" y="-1.567" z="-2.783" 
        compression="0.2" 
        spring="2" 
        sfric="0.5" 
        dfric=".45" 
        retract-time="4" 
        initial-load="0" 
        damp="0.6">
        <control-input control="STEER" axis="/controls/flight/rudder" square="true"/>
        <control-input axis="/controls/gear/gear-down" control="EXTEND"/>
        <control-output control="EXTEND" prop="/gear/gear[2]/position-norm"/>
        <control-speed control="EXTEND" transition-time="4"/>
  </gear>

  <gear x="1.242" y="-1.567" z="-2.718" 
        compression="0.2" 
        spring="3.5" 
        sfric="0.5" 
        dfric=".45" 
        retract-time="4" 
        initial-load="1" 
        damp="10.1">
        <control-input control="BRAKE" axis="/controls/gear/brake-parking" />
        <control-input control="BRAKE" axis="/controls/gear/brake-right" />
        <control-input axis="/controls/gear/gear-down" control="EXTEND"/>
        <control-output control="EXTEND" prop="/gear/gear[3]/position-norm"/>
        <control-speed control="EXTEND" transition-time="4"/>
  </gear>

  <!-- Flotteurs -->
  <!-- Avant 4/5 -->
  <gear x="5.555" y="1.567" z="-2.155" 
        compression="1.5" 
        spring = "15" 
        sfric = "0.85" 
        dfric = "0.75" 
        ignored-by-solver="0"
        on-water="1" 
        on-solid="0" 
        reduce-friction-by-extension="1.15"
        speed-planing="25" 
        spring-factor-not-planing="0.25">
  </gear>

  <gear x="5.555" y="-1.567" z="-2.155" 
        compression="1.5" 
        spring = "15" 
        sfric = "0.85"
        dfric = "0.75" 
        ignored-by-solver="0"
        on-water="1" 
        on-solid="0" 
        reduce-friction-by-extension="1.15"
        speed-planing="25" 
        spring-factor-not-planing="0.25">
  </gear>

  <!-- Central 6/7 -->
  <gear x="1.512" y="1.567" z="-2.355" 
        compression="1.2" 
        spring = "15" 
        sfric = "0.8" 
        dfric = "0.7" 
        ignored-by-solver="0"
        on-water="1" 
        on-solid="0" 
        reduce-friction-by-extension="1.15"
        speed-planing="15" 
        spring-factor-not-planing="0.4">
  </gear>

  <gear x="1.512" y="-1.567" z="-2.355" 
        compression="1.2" 
        spring = "15" 
        sfric = "0.8" 
        dfric = "0.7" 
        ignored-by-solver="0"
        on-water="1" 
        on-solid="0" 
        reduce-friction-by-extension="1.15"
        speed-planing="15" 
        spring-factor-not-planing="0.4">
  </gear>
 
  <!-- Arriere 8/9 -->
  <gear x="-1.746" y="1.567" z="-2.055" 
        compression="1.8" 
        spring = "8" 
        sfric = "0.9" 
        dfric = "0.8" 
        ignored-by-solver="1"
        on-water="1" 
        on-solid="0" 
        reduce-friction-by-extension="0.9"
        speed-planing="25" 
        spring-factor-not-planing="0.85">
  </gear>

  <gear x="-1.746" y="-1.567" z="-2.055" 
        compression="1.8" 
        spring = "8" 
        sfric = "0.9" 
        dfric = "0.8" 
        ignored-by-solver="1"
        on-water="1" 
        on-solid="0" 
        reduce-friction-by-extension="0.9"
        speed-planing="25" 
        spring-factor-not-planing="0.85">
  </gear>

  <!-- 2 more Floats, need to be at this position in the file to do not brake the animation
       "reduce-friction-by-extension" is reduced to get steering control 10/11-->
  <gear x="-2.330" y="1.567" z="-1.856" 
        compression="1.8" 
        spring="8" 
        sfric="0.9" 
        dfric="0.8" 
        ignored-by-solver="1"
        on-water="1" 
        on-solid="0" 
        reduce-friction-by-extension="0.7"
        speed-planing="25" spring-factor-not-planing="3">
        <control-input axis="/controls/gear/water-rudder-pos" control="STEER" invert ="true" square="true"/>
  </gear>

  <gear x="-2.330" y="-1.567" z="-1.856" 
        compression="1.8" 
        spring="8" 
        sfric="0.9" 
        dfric="0.8" 
        ignored-by-solver="1"
        on-water="1"
        on-solid="0" 
        reduce-friction-by-extension="0.7"
        speed-planing="25" spring-factor-not-planing="3">
        <control-input axis="/controls/gear/water-rudder-pos" control="STEER" invert="true" square="true"/>
  </gear>

  <!-- Fuel -->
  <tank x="1.5" y=" 4" z="1.290" capacity="400"/>
  <tank x="0.0" y="0"  z="0.200" capacity="700"/>
  <tank x="1.5" y="-4" z="1.290" capacity="400"/>

  <!-- Mass distribution -->
  <ballast x="5" y="0" z="-3" mass="1500"/>

</airplane>

