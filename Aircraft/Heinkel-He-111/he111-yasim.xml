<?xml version="1.0" encoding="ISO-8859-1"?> 

<!-- Heinkel He 111 - Emmanuel BARANGER (helijah) 2012
   
     sources          : http://en.wikipedia.org/wiki/Heinkel_He_111

     wingspan         : 22.60 m    (  74 ft 2     in)
     length           : 16.40 m    (  53 ft 9 1/2 in)
     height           :  4.00 m    (  13 ft 1 1/2 in)
     max speed        :   440 km/h ( 237.58 kt      )
     empty weight     :  8680 kg   (  19136 lb      )
     engine           : 2 Jumo 211F-2 liquid-cooled inverted V-12 ( 1340 hp each )
-->

<airplane mass="19136">

  <approach speed="60" aoa="4">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="0.4"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="1.0"/>
    <control-setting axis="/controls/engines/engine[1]/throttle" value="0.4"/>
    <control-setting axis="/controls/engines/engine[1]/mixture" value="1.0"/>
    <control-setting axis="/controls/flight/flaps" value="1"/>
    <control-setting axis="/controls/gear/gear-down" value="1"/>
  </approach>

  <cruise speed="210" alt="5000">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="1.0"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="1.0"/>
    <control-setting axis="/controls/engines/engine[1]/throttle" value="1.0"/>
    <control-setting axis="/controls/engines/engine[1]/mixture" value="1.0"/>
    <control-setting axis="/controls/flight/flaps" value="0"/>
    <control-setting axis="/controls/gear/gear-down" value="0"/>
  </cruise>

  <cockpit x="5.659" y="0.272" z="0.874"/>

  <fuselage ax="8.196" ay=" 0.000" az=" 0.138" bx="-8.200" by=" 0.000" bz="-0.029" width="1.695" taper="0.3" midpoint="0.12"/>

  <!-- Left engine -->
  <fuselage ax="7.548" ay=" 2.678" az="-0.422" bx=" 1.898" by=" 2.678" bz="-0.422" width="1.100" taper="0.5" midpoint="0.12"/>

  <!-- Right engine -->
  <fuselage ax="7.548" ay="-2.678" az="-0.422" bx=" 1.898" by="-2.678" bz="-0.422" width="1.100" taper="0.5" midpoint="0.12"/>

  <wing x="3.468" y="0.438" z="-0.592" 
        chord="4.720"
        length="10.800"
        taper="0.5"
        dihedral="6.5"
        camber="0.01"
        sweep="4">
        <stall aoa="20" width="6" peak="2"/>
        <flap0 start="0.05" end="0.56" lift="1.4" drag="1.5"/>
        <flap1 start="0.56" end="0.94" lift="1.6" drag="1.5"/>
      
        <control-input axis="/controls/flight/flaps" control="FLAP0"/>
        <control-input axis="/controls/flight/aileron" control="FLAP1" split="true"/>
        <control-input axis="/controls/flight/aileron-trim" control="FLAP1" split="true"/>
      
        <control-output control="FLAP0" prop="/surface-positions/flaps-pos-norm"/>
        <control-output control="FLAP1" side="left" prop="/surface-positions/left-aileron-pos-norm"/>
        <control-output control="FLAP1" side="right" prop="/surface-positions/right-aileron-pos-norm"/>
  </wing>

  <hstab x="-6.417" y="0.138" z="-0.036" 
         chord="2.637" 
         length="3.828" 
         taper="0.70" 
         dihedral="0"
         sweep="3">
         <stall aoa="18" width="4" peak="2.5"/>
         <flap0 start="0.1" end="0.88" lift="1.6" drag="1.5"/>
       
         <control-input axis="/controls/flight/elevator" control="FLAP0" effectiveness="0.3"/>
         <control-input axis="/controls/flight/elevator-trim" control="FLAP0"/>
       
         <control-output control="FLAP0" prop="/surface-positions/elevator-pos-norm"/>
  </hstab>

  <vstab x="-6.657" y="0" z="0.289" 
         chord="2.602" 
         length="2.469" 
         taper="1">
         <stall aoa="16" width="4" peak="1.5"/>
         <flap0 start="0" end="1" lift="1.3" drag="1.5"/>

         <control-input axis="/controls/flight/rudder" control="FLAP0" effectiveness="2.0" invert="true"/>
         <control-input axis="/controls/flight/rudder-trim" control="FLAP0" invert="true"/>

         <control-output control="FLAP0" prop="/surface-positions/rudder-pos-norm"/>
  </vstab>

  <!-- Engines   source            : http://en.wikipedia.org/wiki/Jumo_211

                 name              : Junkers Jumo 211
                 type              : Twelve-cylinder supercharged liquid-cooled 60-degree inverted V piston aircraft engine
                 power cruise      : 1340 hp at 2200 rpm at 4200 m ( 13780 ft )
                 weight            : 585 kg  ( 1290 lb      )
                 displacement      : 34.99 l ( 2135.2 in cu )
                 Compression ratio : 6.5:1
  -->
  <propeller x="6.430" y="2.678" z="-0.422"
             radius="1.716"
             mass="1290"
             moment="2.5"
             cruise-speed="210"
             cruise-alt="5000"
             cruise-power="1200"
             cruise-rpm="2000"
             takeoff-power="1340"
             takeoff-rpm="2200"
             gear-ratio="0.5"
             contra="1">
             <actionpt x="7.731" y="2.678" z="-0.422"/>
             <piston-engine eng-rpm="2200"
                            eng-power="1340"
                            displacement="2135.2"
                            compression="6.5"/>
             <control-input control="THROTTLE" axis="/controls/engines/engine[0]/throttle"/>
             <control-input control="STARTER" axis="/controls/engines/engine[0]/starter"/>
             <control-input control="MAGNETOS" axis="/controls/engines/engine[0]/magnetos"/>
             <control-input control="MIXTURE" axis="/controls/engines/engine[0]/mixture"/>
  </propeller>

  <propeller x="6.430" y="-2.678" z="-0.422"
             radius="1.716"
             mass="1290"
             moment="2.5"
             cruise-speed="210"
             cruise-alt="5000"
             cruise-power="1200"
             cruise-rpm="2000"
             takeoff-power="1340"
             takeoff-rpm="2200"
             gear-ratio="0.5"
             contra="1">
             <actionpt x="7.731" y="-2.678" z="-0.422"/>
             <piston-engine eng-rpm="2200"
                            eng-power="1340"
                            displacement="2135.2"
                            compression="6.5"/>
             <control-input control="THROTTLE" axis="/controls/engines/engine[1]/throttle"/>
             <control-input control="STARTER" axis="/controls/engines/engine[1]/starter"/>
             <control-input control="MAGNETOS" axis="/controls/engines/engine[1]/magnetos"/>
             <control-input control="MIXTURE" axis="/controls/engines/engine[1]/mixture"/>
  </propeller>

  <!-- left main -->
  <gear x="4.916" y="2.660" z="-2.811" 
        compression="0.4"
        retract-time="5"
        spring="0.8"
        damp="1.2"
        sfric="1"
        dfric="1">
        <control-input axis="/controls/gear/brake-left" control="BRAKE"/>
        <control-input axis="/controls/gear/brake-parking" control="BRAKE"/>
        <control-input axis="/controls/gear/gear-down" control="EXTEND"/>

        <control-speed control="EXTEND" transition-time="5.1"/>

        <control-output control="EXTEND" prop="/gear/gear[0]/position-norm"/>
  </gear>

  <!-- right main -->
  <gear x="4.916" y="-2.660" z="-2.811" 
        compression="0.4"
        retract-time="5"
        spring="0.8"
        damp="1.2"
        sfric="1"
        dfric="1">
        <control-input axis="/controls/gear/brake-left" control="BRAKE"/>
        <control-input axis="/controls/gear/brake-parking" control="BRAKE"/>
        <control-input axis="/controls/gear/gear-down" control="EXTEND"/>

        <control-speed control="EXTEND" transition-time="5.1"/>

        <control-output control="EXTEND" prop="/gear/gear[1]/position-norm"/>
  </gear>

  <!-- Tail wheel; has castering selectable by a wheel lock -->
  <gear x="-6.419" y="0" z="-0.942"
        compression="0.2"
        retract-time="5"
        spring="0.8"
        damp="1.2"
        sfric="1"
        dfric="1">
        <control-input axis="/controls/gear/tailwheel-lock" src0="0" src1="1" dst0="1" dst1="0" control="CASTERING"/>
        <control-input axis="/controls/gear/gear-down" control="EXTEND"/>    

        <control-speed control="EXTEND" transition-time="5.1"/>

        <control-output control="EXTEND" prop="/gear/gear[2]/position-norm"/>
  </gear>

  <tank x=" 3.240" y=" 3.400" z="-0.272" capacity="1000"/>
  <tank x="-0.200" y=" 0.000" z=" 0.000" capacity="1500"/>
  <tank x= "3.240" y="-3.400" z="-0.272" capacity="1000"/>
<!--
  <ballast x="2" y="0" z="-1.5" mass="1500"/>
-->
</airplane>

