<?xml version="1.0"?>
<!--
************************************************************************
P-51D simulation config. This files ties together all the components
used by FGFS to represent the P-51D. Components include the flight data 
model, instrument panel, and external 3D model.

March 2003 Jim Wilson, jimw@kelcomaine.com
************************************************************************
-->

<PropertyList>

 <sim>

  <description>P-51D</description>
  <author>Jim Wilson</author>
  <status>production depreciated</status>
  
  <flight-model>yasim</flight-model>
  <aero>p51d</aero>
  <fuel-fraction>0.50</fuel-fraction>
  
  <startup>
     <splash-texture>Aircraft/p51d/P-51D-splash.png</splash-texture>
  </startup>

  <virtual-cockpit archive="y">true</virtual-cockpit>
  <allow-toggle-cockpit archive="y">true</allow-toggle-cockpit>

  <sound>
   <path>Aircraft/p51d/p51d-sound.xml</path>
  </sound>

  <panel>
  <path>Aircraft/Generic/Panels/generic-vfr-panel.xml</path>
  <visibility>false</visibility>
  </panel>

  <model>
   <path>Aircraft/p51d/Models/p51d-jw.xml</path>
  </model> 

  <view n="0">
   <internal archive="y">true</internal>
   <config>
    <x-offset-m archive="y" type="double">0.0</x-offset-m>
    <y-offset-m archive="y" type="double">0.730</y-offset-m>
    <z-offset-m archive="y" type="double">4.495</z-offset-m>
    <pitch-offset-deg>-17.5</pitch-offset-deg>
   </config>
  </view>

  <view n="1">
   <config>
    <target-z-offset-m archive="y" type="double">3.949</target-z-offset-m>
   </config>
  </view>

  <view n="2">
   <config>
    <target-z-offset-m archive="y" type="double">3.949</target-z-offset-m>
   </config>
  </view>

  <view n="3">
   <config>
    <target-z-offset-m archive="y" type="double">3.949</target-z-offset-m>
   </config>
  </view>

  <view n="4">
   <config>
    <target-z-offset-m archive="y" type="double">3.949</target-z-offset-m>
   </config>
  </view>

  <view n="5">
   <config>
    <target-z-offset-m archive="y" type="double">3.949</target-z-offset-m>
   </config>
  </view>

  <view n="6">
   <config>
    <target-z-offset-m archive="y" type="double">3.949</target-z-offset-m>
   </config>
  </view>

 <autopilot>
   <config>
     <min-climb-speed-kt type="float">70.0</min-climb-speed-kt>
     <best-climb-speed-kt type="float">75.0</best-climb-speed-kt>
     <target-climb-rate-fpm type="float">500.0</target-climb-rate-fpm>
     <target-descent-rate-fpm type="float">1000.0</target-descent-rate-fpm>
     <elevator-adj-factor type="float">4000.0</elevator-adj-factor>
     <integral-contribution type="float">0.01</integral-contribution>
     <zero-pitch-throttle type="float">0.45</zero-pitch-throttle>
     <zero-pitch-trim-full-throttle type="float">0.14</zero-pitch-trim-full-throttle>
   </config>
 </autopilot>

  <hud>
   <enable3d>false</enable3d>
  </hud>

  <help>

    <line>Maximum Speed: 437 mph</line>
    <line>Cruise Speed: 363 mph</line>
    <line>Landing Speed: 100 mph</line>
    <line>Initial Climb Rate: 3475 feet per minute</line>
    <line>Sustained Climb Speed: 175 mph</line>
    <line>Service Ceiling: 41,900 (with 2nd Stage booster)</line>
    <line>Stall Speed (9000 lbs) Gear/Flaps Up: 102mph</line>
    <line>Stall Speed (9000 lbs) Gear/Flaps Down: 95mph</line>
    <text>TAKE OFF___________________________________________________
Set prop pitch to full increased rpm.
Start engine with throttle at idle.
Set BOOST stage one (CTRL-b keybinding cycles through stage 0 (off), 1, and 2).  Default startup for p51-d should have stage one already on.
Set throttle to 40 inHG Manifold Pressure (MP).
Be ready to actuate rudder.  When the tail first lifts, the torque will pull the nose to the left.
After tail lifts off slowing move throttle to full open which with stage 1 booster engaged should be about 61 inHG Manifold Pressure.
Hang on.  Stay on top of rudder control with small adjustments or you'll do a ground loop.
Rotate at 150 mph or so.

CLIMB______________________________________________________
Back off MP to 46 inHG.
Adjust propeller pitch to 2700 rpm.
Keep adding throttle as you climb to maintain 46 inHG until you hit full throttle.
At 18,000 feet turn on second stage (hit CTRL-b) and back off throttle to 46 inHG.

FLYING_____________________________________________________
Trim and Cruise at about 2400 rpm.
Do not exceed 2700 rpm sustained.
Do not exceed 3000 rpm military power (aerobatics)
Do not exceed 3500 rpm in dives.
Do not exceed 61 inHG Mainfold Pressure (military power), except 71 inHG for maximum of 7 minutes (war emergency power).  Note that war emergency power is not for flying fast,  rather it is for dogfighting at &lt; 200mph.

LANDING____________________________________________________
A single long sweeping turn from the downwind leg into a short approach seems to work best.  Make sure you are below 250mph before starting the turn.  Use forward slip as necessary to bleed off speed and altitude at the same time.
Start applying flaps at 250mph,  gear at 175mph, and full flaps at 165mph or less.

Excessive braking can cause you to nose over.  Some recommend raising flaps immediately after touchdown.
    </text>
  </help>

 </sim>

 <engines>
  <engine>
   <rpm type="double">700</rpm>
   <!-- fake unimplemented settings -->
   <fuel-pump-psi type="double">18</fuel-pump-psi>
   <oil-pressure-psi type="double">75</oil-pressure-psi>
  </engine>
 </engines>

 <consumables>
  <fuel>
   <tank n="0">
    <level-gal_us>20</level-gal_us>
   </tank>
   <tank n="1">
    <level-gal_us>20</level-gal_us>
   </tank>
  </fuel>
 </consumables>

 <!-- set prop for the manual "bookmark" arrow on the remote compas -->
 <instrumentation>
    <magnetic-compass>
       <bookmark-heading-deg type="double">43.0</bookmark-heading-deg>
    </magnetic-compass>
 </instrumentation>

 <!-- set up control properties for YASim...default to first stage boost on -->
 <controls>
   <engines>
      <engine n="0">
        <magnetos>3</magnetos>
				<blower type="int">0</blower>
        <boost type="double">0.51</boost><!--0.51-->
      </engine>
   </engines>
 </controls>
 <nasal>
   <p51d>
      <file>Aircraft/p51d/Nasal/p51.nas</file>
   </p51d>
 </nasal>
</PropertyList>




