<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="http://jsbsim.sourceforge.net/JSBSim.xsl"?>
<fdm_config name="HondaJet" version="2.0" release="ALPHA"
	    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	    xsi:noNamespaceSchemaLocation="http://jsbsim.sourceforge.net/JSBSim.xsd">

  <fileheader>
    <author> Tatsuhiro Nishioka </author>
    <filecreationdate> 2008-01-21 </filecreationdate>
    <version>$Revision$</version>
    <description> HondaJet </description>
    <reference refID="1" author="Michimasa Fujimoto" title="Design and Development of the HondaJet" date="May/2005"/>
  </fileheader>

  <!--
      File:     HondaJet.xml
      Inputs:
      name:          HondaJet
      type:          two-engine transonic transport
      max weight:    8709.75 lb << actually 9120lb, but adjusted to get accurate empty weight
      wing span:     44.91689 ft
      length:        46.49177 ft
      wing area:     215.3 sq-ft
      gear type:     tricycle
      retractable?:  yes
      # engines:     2
      engine type:   turbine
      engine layout: wings
      yaw damper?    no

      Outputs:
      wing loading:  40.45 lb/sq-ft
      CL-alpha:      4.4 per radian
      CL-0:          0.2
      CL-max:        1.2
      CD-0:          0.02
      K:             0.043

      http://www.aopa.org/pilot/features/2005/feat0508.html
      http://hondajet.honda.com/pdf/tech_papers/Journal_of_Aircraft_Vol40_No6_P1177_P1184_Wave_Drag_OTWEM.pdf
      http://hondajet.honda.com/_pdf/hondajet_Specifications.pdf
      http://hondajet.honda.com/pdf/tech_papers/Journal_of_Aircraft_Vol42_No3_P755_P764_HondaJet.pdf
    -->

  <metrics>
    <wingarea  unit="FT2"> 215.30 </wingarea>
    <wingspan  unit="FT" >  44.92 </wingspan>
    <wing_incidence>         2.00 </wing_incidence>
    <chord     unit="FT" >   5.18 </chord>
    <htailarea unit="FT2">  51.70 </htailarea>
    <htailarm  unit="FT" >  19.12 </htailarm>
    <vtailarea unit="FT2">  48.00 </vtailarea>
    <vtailarm  unit="FT" >  11.37 </vtailarm>
    <location name="AERORP" unit="IN">
      <x> -22.90 </x>
      <y>   0.00 </y>
      <z> -46.45 </z>
    </location>
    <location name="EYEPOINT" unit="IN">
      <x> -131.49 </x>
      <y>   -0.00 </y>
      <z>    2.75 </z>
    </location>

    <!-- 
	 VRP should be the relative coordinate from the origin of FDM to the center of 3D model 
	 the origin of FDM is the center of 3D Model for HondaJet, so VRP is (0,0,0)
      -->
    <location name="VRP" unit="IN">
      <x> 0 </x>
      <y> 0 </y>
      <z> 0 </z>
    </location>
  </metrics>

  <mass_balance>
    <ixx unit="SLUG*FT2">  5116 </ixx>
    <iyy unit="SLUG*FT2"> 12664 </iyy>
    <izz unit="SLUG*FT2"> 17934 </izz>
    <emptywt unit="LBS" >  5226 </emptywt>
    <location name="CG" unit="IN">
      <x> -21.74</x>
      <y>  0.00</y>
      <z>-46.45</z>
    </location>
  </mass_balance>

  <ground_reactions>

    <contact type="BOGEY" name="NOSE">
      <location unit="IN">
	<x> -190.0 </x>
	<y>   0.00 </y>
	<z>  -82.92</z>
      </location>
      <static_friction>  0.80 </static_friction>
      <dynamic_friction> 0.50 </dynamic_friction>
      <rolling_friction> 0.02 </rolling_friction>
      <spring_coeff unit="LBS/FT">      2612.92 </spring_coeff>
      <damping_coeff unit="LBS/FT/SEC">  870.98 </damping_coeff>
      <max_steer unit="DEG"> 5.00 </max_steer>
      <brake_group>NONE</brake_group>
      <retractable>RETRACT</retractable>
    </contact>

    <contact type="BOGEY" name="LEFT_MAIN">
      <location unit="IN">
	<x>  15.34 </x>
	<y> -60.63 </y>
	<z> -82.92 </z>
      </location>
      <static_friction>  0.80 </static_friction>
      <dynamic_friction> 0.50 </dynamic_friction>
      <rolling_friction> 0.02 </rolling_friction>
      <spring_coeff unit="LBS/FT">      8709.75 </spring_coeff>
      <damping_coeff unit="LBS/FT/SEC"> 1741.95 </damping_coeff>
      <max_steer unit="DEG">0</max_steer>
      <brake_group>LEFT</brake_group>
      <retractable>RETRACT</retractable>
    </contact>

    <contact type="BOGEY" name="RIGHT_MAIN">
      <location unit="IN">
	<x>  15.34 </x>
	<y>  60.63 </y>
	<z> -82.92 </z>
      </location>
      <static_friction>  0.80 </static_friction>
      <dynamic_friction> 0.50 </dynamic_friction>
      <rolling_friction> 0.02 </rolling_friction>
      <spring_coeff unit="LBS/FT">      8709.75 </spring_coeff>
      <damping_coeff unit="LBS/FT/SEC"> 1741.95 </damping_coeff>
      <max_steer unit="DEG">0</max_steer>
      <brake_group>RIGHT</brake_group>
      <retractable>RETRACT</retractable>
    </contact>

    <contact type="STRUCTURE" name="LEFT_WING">
      <location unit="IN">
	<x>    3.94 </x>
	<y> -225.59 </y>
	<z> -46.45 </z>
      </location>
      <static_friction>  0.80 </static_friction>
      <dynamic_friction> 0.50 </dynamic_friction>
      <spring_coeff unit="LBS/FT">       8709.75 </spring_coeff>
      <damping_coeff unit="LBS/FT/SEC">  1741.95 </damping_coeff>
    </contact>

    <contact type="STRUCTURE" name="RIGHT_WING">
      <location unit="IN">
	<x>    3.94 </x>
	<y>  225.59 </y>
	<z> -46.45 </z>
      </location>
      <static_friction>  0.80 </static_friction>
      <dynamic_friction> 0.50 </dynamic_friction>
      <spring_coeff unit="LBS/FT">       8709.75 </spring_coeff>
      <damping_coeff unit="LBS/FT/SEC">  1741.95 </damping_coeff>
    </contact>
  </ground_reactions>

  <propulsion>

    <engine file="HF120">
      <location unit="IN">
	<x>  53.15 </x>
	<y> -59.05 </y>
	<z>  -9.84 </z>
      </location>
      <orient unit="DEG">
	<pitch> 0.00 </pitch>
	<roll>  0.00 </roll>
	<yaw>   0.00 </yaw>
      </orient>
      <feed>0</feed>
      <feed>1</feed>
      <feed>2</feed>
      <thruster file="direct">
	<location unit="IN">
	  <x>  95.67 </x>
	  <y> -59.05 </y>
	  <z>  -9.84 </z>
	</location>
	<orient unit="DEG">
	  <pitch> 0.00 </pitch>
	  <roll>  0.00 </roll>
	  <yaw>   0.00 </yaw>
	</orient>
      </thruster>
    </engine>

    <engine file="HF120">
      <location unit="IN">
	<x> 53.15 </x>
	<y> 59.05 </y>
	<z> -9.84 </z>
      </location>
      <orient unit="DEG">
	<pitch> 0.00 </pitch>
	<roll>  0.00 </roll>
	<yaw>   0.00 </yaw>
      </orient>
      <feed>0</feed>
      <feed>1</feed>
      <feed>2</feed>
      <thruster file="direct">
	<location unit="IN">
	  <x> 95.67 </x>
	  <y> 59.05 </y>
	  <z> -9.84 </z>
	</location>
	<orient unit="DEG">
	  <pitch> 0.00 </pitch>
	  <roll>  0.00 </roll>
	  <yaw>   0.00 </yaw>
	</orient>
      </thruster>
    </engine>

    <tank type="FUEL" number="0">
      <location unit="IN">
	<x>   0.00 </x>
	<y>   0.00 </y>
	<z>  -46.45 </z>
      </location>
      <capacity unit="LBS"> 1800.00 </capacity>
      <contents unit="LBS">  900.00 </contents>
    </tank>

    <tank type="FUEL" number="1">
      <location unit="IN">
	<x>   0.00 </x>
	<y> -65.35 </y>
	<z> -46.45 </z>
      </location>
      <capacity unit="LBS"> 150.00 </capacity>
      <contents unit="LBS">  75.00 </contents>
    </tank>

    <tank type="FUEL" number="2">
      <location unit="IN">
	<x>   0.00 </x>
	<y>  65.35 </y>
	<z> -46.45 </z>
      </location>
      <capacity unit="LBS"> 150.00 </capacity>
      <contents unit="LBS">  75.00 </contents>
    </tank>

  </propulsion>

  <flight_control name="FCS: HondaJet">

    <channel name="Pitch">
      <summer name="Pitch Trim Sum">
	<input>fcs/elevator-cmd-norm</input>
	<input>fcs/pitch-trim-cmd-norm</input>
	<clipto>
          <min> -1 </min>
          <max>  1 </max>
	</clipto>
      </summer>

      <aerosurface_scale name="Elevator Control">
	<input>fcs/pitch-trim-sum</input>
	<range>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</range>
	<output>fcs/elevator-pos-rad</output>
      </aerosurface_scale>

      <aerosurface_scale name="elevator normalization">
	<input>fcs/elevator-pos-rad</input>
	<domain>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</domain>
	<range>
          <min> -1 </min>
          <max>  1 </max>
	</range>
	<output>fcs/elevator-pos-norm</output>
      </aerosurface_scale>

    </channel>

    <channel name="Roll">
      <summer name="Roll Trim Sum">
	<input>fcs/aileron-cmd-norm</input>
	<input>fcs/roll-trim-cmd-norm</input>
	<clipto>
          <min> -1 </min>
          <max>  1 </max>
	</clipto>
      </summer>

      <aerosurface_scale name="Left Aileron Control">
	<input>fcs/roll-trim-sum</input>
	<range>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</range>
	<output>fcs/left-aileron-pos-rad</output>
      </aerosurface_scale>

      <aerosurface_scale name="Right Aileron Control">
	<input>fcs/roll-trim-sum</input>
	<range>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</range>
	<output>fcs/right-aileron-pos-rad</output>
      </aerosurface_scale>

      <aerosurface_scale name="left aileron normalization">
	<input>fcs/left-aileron-pos-rad</input>
	<domain>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</domain>
	<range>
          <min> -1 </min>
          <max>  1 </max>
	</range>
	<output>fcs/left-aileron-pos-norm</output>
      </aerosurface_scale>

      <aerosurface_scale name="right aileron normalization">
	<input>-fcs/right-aileron-pos-rad</input>
	<domain>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</domain>
	<range>
          <min> -1 </min>
          <max>  1 </max>
	</range>
	<output>fcs/right-aileron-pos-norm</output>
      </aerosurface_scale>

    </channel>

    <channel name="Yaw">
      <summer name="Rudder Command Sum">
	<input>fcs/rudder-cmd-norm</input>
	<input>fcs/yaw-trim-cmd-norm</input>
	<clipto>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</clipto>
      </summer>

      <aerosurface_scale name="Rudder Control">
	<input>fcs/rudder-command-sum</input>
	<range>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</range>
	<output>fcs/rudder-pos-rad</output>
      </aerosurface_scale>

      <aerosurface_scale name="rudder normalization">
	<input>fcs/rudder-pos-rad</input>
	<domain>
          <min> -0.35 </min>
          <max>  0.35 </max>
	</domain>
	<range>
          <min> -1 </min>
          <max>  1 </max>
	</range>
	<output>fcs/rudder-pos-norm</output>
      </aerosurface_scale>

    </channel>

    <channel name="Flaps">
      <kinematic name="Flaps Control">
	<input>fcs/flap-cmd-norm</input>
	<traverse>
	  <setting>
            <position>  0 </position>
            <time>      0 </time>
	  </setting>
	  <setting>
            <position> 0.314 </position>
            <time>      4 </time>
	  </setting>
	  <setting>
            <position> 1.0 </position>
            <time>     8.7 </time>
	  </setting>
	</traverse>
	<output>fcs/flap-pos-norm</output>
      </kinematic>

      <aerosurface_scale name="flap normalization">
	<input>fcs/flap-pos-norm</input>
	<domain>
          <min>  0 </min>
          <max>  1 </max>
	</domain>
	<range>
          <min> 0 </min>
          <max> 50 </max>
	</range>
	<output>fcs/flap-pos-deg</output>
      </aerosurface_scale>

    </channel>

    <channel name="Landing Gear">
      <kinematic name="Gear Control">
	<input>gear/gear-cmd-norm</input>
	<traverse>
	  <setting>
            <position> 0 </position>
            <time>     0 </time>
	  </setting>
	  <setting>
            <position> 1 </position>
            <time>     5 </time>
	  </setting>
	</traverse>
	<output>gear/gear-pos-norm</output>
      </kinematic>

    </channel>

    <channel name="Speedbrake">
      <kinematic name="Speedbrake Control">
	<input>fcs/speedbrake-cmd-norm</input>
	<traverse>
	  <setting>
            <position> 0 </position>
            <time>     0 </time>
	  </setting>
	  <setting>
            <position> 1 </position>
            <time>     1 </time>
	  </setting>
	</traverse>
	<output>fcs/speedbrake-pos-norm</output>
      </kinematic>

    </channel>

  </flight_control>

  <aerodynamics>

    <axis name="LIFT">

      <function name="aero/coefficient/CLalpha">
	<description>Lift_due_to_alpha</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <table>
            <independentVar lookup="row">aero/alpha-deg</independentVar>
            <tableData>
              -5.00 -0.450
              0.00 0.000
              5.00 0.475
              10.00 0.920
              11.00 1.000
              12.00 1.070
              13.00 1.133
              14.00 1.185
              15.00 1.225
              16.00 1.210
              17.00 1.187
              18.00 1.150
              19.00 1.125
              20.00 1.100
              45.00 0.600
            </tableData>
          </table>
	</product>
      </function>

      <function name="aero/coefficient/dCLflap">
	<description>Delta_Lift_due_to_flaps</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <table>
            <independentVar lookup="row">fcs/flap-pos-deg</independentVar>
            <tableData>
              0.00 0.00
              15.70 0.35
              50.00 0.89
            </tableData> 
          </table>
	</product>
      </function>

      <function name="aero/coefficient/dCLsb">
	<description>Delta_Lift_due_to_speedbrake</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>fcs/speedbrake-pos-norm</property>
          <value>-0.1</value>
	</product>
      </function>

      <function name="aero/coefficient/CLde">
	<description>Lift_due_to_Elevator_Deflection</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>fcs/elevator-pos-rad</property>
          <value>0.2</value>
	</product>
      </function>

    </axis>

    <axis name="DRAG">

      <function name="aero/coefficient/CD0">
	<description>Drag_at_zero_lift</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <table>
            <independentVar lookup="row">aero/alpha-rad</independentVar>
            <tableData>
              -1.57    1.500
              -0.26    0.026
              0.00    0.020
              0.26    0.026
              1.57    1.500
            </tableData>
          </table>
	</product>
      </function>

      <function name="aero/coefficient/CDi">
	<description>Induced_drag</description>
        <product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>aero/cl-squared</property>
          <value>0.043</value>
        </product>
      </function>

<!--
     CDmach around 0.70 differ from ones on the paper.
     This is for increasing max speed at FL300
 -->
      <function name="aero/coefficient/CDmach">
	<description>Drag_due_to_mach</description>
        <product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <table>
            <independentVar lookup="row">velocities/mach</independentVar>
            <tableData>
              0.00      0.000
              0.65      0.0225
              0.70      0.0285
              0.71      0.0310
              0.72      0.0325
              0.73      0.0326
              0.74      0.0326
              0.75      0.0328
              0.76      0.0332
              0.77      0.0337
              0.78      0.0345
              0.79      0.0360
              0.80      0.0380
              0.81      0.0407
              0.82      0.0442
              0.83      0.0476
              0.84      0.0517
            </tableData>
          </table>
        </product>
      </function>

      <function name="aero/coefficient/CDflap">
	<description>Drag_due_to_flaps</description>
        <product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>fcs/flap-pos-deg</property>
          <value> 0.00197 </value>
        </product>
      </function>

      <function name="aero/coefficient/CDgear">
	<description>Drag_due_to_gear</description>
        <product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>gear/gear-pos-norm</property>
          <value>0.015</value>
        </product>
      </function>

      <function name="aero/coefficient/CDsb">
	<description>Drag_due_to_speedbrakes</description>
        <product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>fcs/speedbrake-pos-norm</property>
          <value>0.02</value>
        </product>
      </function>

      <function name="aero/coefficient/CDbeta">
	<description>Drag_due_to_sideslip</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <table>
            <independentVar lookup="row">aero/beta-rad</independentVar>
            <tableData>
              -1.57    1.230
              -0.26    0.050
              0.00    0.000
              0.26    0.050
              1.57    1.230
            </tableData>
          </table>
	</product>
      </function>

      <function name="aero/coefficient/CDde">
	<description>Drag_due_to_Elevator_Deflection</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>fcs/elevator-pos-norm</property>
          <value>0.04</value>
	</product>
      </function>

    </axis>

    <axis name="SIDE">

      <function name="aero/coefficient/CYb">
	<description>Side_force_due_to_beta</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>aero/beta-rad</property>
          <value>-1</value>
	</product>
      </function>

    </axis>

    <axis name="ROLL">

      <function name="aero/coefficient/Clb">
	<description>Roll_moment_due_to_beta</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>aero/beta-rad</property>
          <value>-0.1</value>
	</product>
      </function>

      <function name="aero/coefficient/Clp">
	<description>Roll_moment_due_to_roll_rate</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>aero/bi2vel</property>
          <property>velocities/p-aero-rad_sec</property>
          <value>-0.4</value>
	</product>
      </function>

      <function name="aero/coefficient/Clr">
	<description>Roll_moment_due_to_yaw_rate</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>aero/bi2vel</property>
          <property>velocities/r-aero-rad_sec</property>
          <value>0.15</value>
	</product>
      </function>

      <function name="aero/coefficient/Clda">
	<description>Roll_moment_due_to_aileron</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>fcs/left-aileron-pos-rad</property>
          <table>
            <independentVar lookup="row">velocities/mach</independentVar>
            <tableData>
              0.0    0.100
              2.0    0.033
            </tableData>
          </table>
	</product>
      </function>

      <function name="aero/coefficient/Cldr">
	<description>Roll_moment_due_to_rudder</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>fcs/rudder-pos-rad</property>
          <value>0.01</value>
	</product>
      </function>

    </axis>

    <axis name="PITCH">

      <function name="aero/coefficient/Cmalpha">
	<description>Pitch_moment_due_to_alpha</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/cbarw-ft</property>
          <property>aero/alpha-rad</property>
          <value>-0.6</value>
	</product>
      </function>

      <function name="aero/coefficient/Cmde">
	<description>Pitch_moment_due_to_elevator</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/cbarw-ft</property>
          <property>fcs/elevator-pos-rad</property>
          <table>
            <independentVar lookup="row">velocities/mach</independentVar>
            <tableData>
              0.0     -1.200
              2.0     -0.300
            </tableData>
          </table>
	</product>
      </function>

      <function name="aero/coefficient/Cmq">
	<description>Pitch_moment_due_to_pitch_rate</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/cbarw-ft</property>
          <property>aero/ci2vel</property>
          <property>velocities/q-aero-rad_sec</property>
          <value>-17</value>
	</product>
      </function>

      <function name="aero/coefficient/Cmadot">
	<description>Pitch_moment_due_to_alpha_rate</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/cbarw-ft</property>
          <property>aero/ci2vel</property>
          <property>aero/alphadot-rad_sec</property>
          <value>-6</value>
	</product>
      </function>

    </axis>

    <axis name="YAW">

      <function name="aero/coefficient/Cnb">
	<description>Yaw_moment_due_to_beta</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>aero/beta-rad</property>
          <value>0.12</value>
	</product>
      </function>

      <function name="aero/coefficient/Cnr">
	<description>Yaw_moment_due_to_yaw_rate</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>aero/bi2vel</property>
          <property>velocities/r-aero-rad_sec</property>
          <value>-0.15</value>
	</product>
      </function>

      <function name="aero/coefficient/Cndr">
	<description>Yaw_moment_due_to_rudder</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>fcs/rudder-pos-rad</property>
          <value>-0.1</value>
	</product>
      </function>

      <function name="aero/coefficient/Cnda">
	<description>Adverse_yaw</description>
	<product>
          <property>aero/qbar-psf</property>
          <property>metrics/Sw-sqft</property>
          <property>metrics/bw-ft</property>
          <property>fcs/left-aileron-pos-rad</property>
          <value>0</value>
	</product>
      </function>

    </axis>

  </aerodynamics>

</fdm_config>
