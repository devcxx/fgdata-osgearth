<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- D.H.89       : Updated with Melchior's script
                    2011 Emmanuel BARANGER

     Source       : http://en.wikipedia.org/wiki/De_Havilland_Dragon_Rapide
                 
     Wingspan     :   48 ft 0 in ( 14.6 m  )
     Length       :   34 ft 6 in ( 10.5 m  )
     Height       :   10 ft 3 in (  3.1 m  )
     Empty weight : 3230 lb      ( 1460 kg )
     Engine       : 2 de Havilland Gipsy Six inline engine (200 hp each)
     Max speed    :  253 km/h    ( 136 ky  ) at 1000 ft ( 305 m )
-->

<airplane mass="3230">

  <approach speed="55" aoa="12">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="0.5"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="1"/>
    <control-setting axis="/controls/engines/engine[1]/throttle" value="0.5"/>
    <control-setting axis="/controls/engines/engine[1]/mixture" value="1"/>
    <control-setting axis="/controls/flight/flaps" value="1"/>
  </approach>

  <cruise speed="130" alt="5000">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="1"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="1"/>
    <control-setting axis="/controls/engines/engine[1]/throttle" value="1"/>
    <control-setting axis="/controls/engines/engine[1]/mixture" value="1"/>
    <control-setting axis="/controls/flight/flaps" value="0"/>
  </cruise>

  <cockpit x="4.022" y="0" z="0.468"/>

  <fuselage ax="5.261" ay="0" az="-0.246" bx="-4.8" by="0" bz="0.2" width="1.2" taper="0.5" midpoint="0.1"/>

  <!-- lower wing -->
  <wing x="1.651" y="0.67" z="-0.767" 
        chord="1.857" 
        length="6.577" 
        camber="0.28"
        taper="0.29" 
        sweep="-3.0" 
        dihedral="1" 
        twist="-3">
        <stall aoa="12" width="6" peak="1.5"/>
        <flap0 start="0.01" end="0.36" lift="1.5" drag="1.2"/>
        <flap1 start="0.60" end="0.97" lift="1.3" drag="1.1"/>

        <control-input axis="/controls/flight/flaps" control="FLAP0"/>
        <control-input axis="/controls/flight/aileron" control="FLAP1" split="true"/>
        <control-input axis="/controls/flight/aileron-trim" control="FLAP1" split="true"/>

        <control-output control="FLAP0" prop="/surface-positions/flap-pos-norm"/>
        <control-output control="FLAP1" side="left" prop="/surface-positions/left-aileron-pos-norm"/>
        <control-output control="FLAP1" side="right" prop="/surface-positions/right-aileron-pos-norm"/>

        <control-speed control="FLAP0" transition-time="5"/>
  </wing>

  <!-- upper wing -->
  <mstab x="1.834" y="0.67" z="0.694" 
         chord="1.857" 
         length="6.577" 
         camber="0.1"
         taper="0.29" 
         sweep="-3.0" 
         dihedral="2" 
         twist="-3">
         <stall aoa="12" width="6" peak="1.5"/>
         <flap0 start="0.60" end="0.97" lift="1.3" drag="1.1"/>

         <control-input axis="/controls/flight/aileron" control="FLAP0" split="true"/>
         <control-input axis="/controls/flight/aileron-trim" control="FLAP0" split="true"/>

         <control-output control="FLAP0" side="left" prop="/surface-positions/left-aileron-pos-norm"/>
         <control-output control="FLAP0" side="right" prop="/surface-positions/right-aileron-pos-norm"/>
  </mstab>

  <!-- elevator -->
  <hstab x="-4.108" y="0.078" z="0.357" 
         chord="1.460" 
         length="1.933"
         taper="0.8" 
         sweep="0" 
         incidence="0">
         <stall aoa="18" width="3" peak="1.5"/>
         <flap0 start="0" end="1" lift="1.8" drag="1.7"/>

         <control-input axis="/controls/flight/elevator" control="FLAP0" />
         <control-input axis="/controls/flight/elevator-trim" control="FLAP0"/>

         <control-output control="FLAP0" prop="/surface-positions/elevator-pos-norm"/>
  </hstab>

  <!-- rudder -->
  <vstab x="-4.360" y="0" z="0" 
         length="2.121"
         chord="1.877" 
         taper="0.462" 
         sweep="12">
         <stall aoa="14" width="3" peak="1.5"/>
         <flap0 start="0" end="1" lift="1.5" drag="1.2"/>

         <control-input axis="/controls/flight/rudder" control="FLAP0" invert="true"/>
         <control-input axis="/controls/flight/rudder-trim" control="FLAP0" invert="true"/>

         <control-output control="FLAP0" prop="/surface-positions/rudder-pos-norm"/>
  </vstab>

  <!-- Engines   source            : http://en.wikipedia.org/wiki/De_Havilland_Gipsy_Six

                 Name              : de Havilland Gipsy Six
                 Type              : 6-cylinder air-cooled inverted inline piston aircraft engine
                 Displacement      : 560.6 in3 ( 9.186 L )
                 Power cruise      : 200 hp at 2350 rpm (on 70 octane fuel)
                 Weight            : 468 lb ( 213 kg )
                 Compression       : 5.25:1
  -->
  <propeller x="3.0" y="1.819" z="-0.75"
             mass="468" 
             moment="5" 
             radius="1.0625"
             cruise-power="185"
             cruise-speed="115"
             cruise-rpm="2100"
             cruise-alt="2000"
             contra="1">
             <actionpt x="3.886" y="1.819" z="-0.75"/>
             <control-input axis="/controls/engines/engine[0]/propeller-pitch" control="ADVANCE" />
             <piston-engine eng-power="200" 
                            eng-rpm="2350"
                            turbo-mul="1.5"
                            compression="5.25"
                            displacement="560.6">
                            <control-input axis="/controls/engines/engine[0]/throttle" control="THROTTLE"/>
                            <control-input axis="/controls/engines/engine[0]/starter" control="STARTER"/>
                            <control-input axis="/controls/engines/engine[0]/magnetos" control="MAGNETOS"/>
                            <control-input axis="/controls/engines/engine[0]/mixture" control="MIXTURE"/>
             </piston-engine>
  </propeller>

  <propeller x="3.0" y="-1.819" z="-0.75"
             mass="468" 
             moment="5" 
             radius="1.0625"
             cruise-power="185"
             cruise-speed="115"
             cruise-rpm="2100"
             cruise-alt="2000"
             contra="1">
             <actionpt x="3.886" y="-1.819" z="-0.75"/>
             <control-input axis="/controls/engines/engine[1]/propeller-pitch" control="ADVANCE" />
             <piston-engine eng-power="200" 
                            eng-rpm="2350"
                            turbo-mul="1.5"
                            compression="5.25"
                            displacement="560.6">
                            <control-input axis="/controls/engines/engine[1]/throttle" control="THROTTLE"/>
                            <control-input axis="/controls/engines/engine[1]/starter" control="STARTER"/>
                            <control-input axis="/controls/engines/engine[1]/magnetos" control="MAGNETOS"/>
                            <control-input axis="/controls/engines/engine[1]/mixture" control="MIXTURE"/>
             </piston-engine>
  </propeller>

  <gear x="2.591" y="1.825" z="-2.125"
        compression="0.1" 
        spring="1"
        damp="10"
        dfric="0.9"
        sfric ="0.5">
        <control-input axis="/controls/gear/brake-parking" control="BRAKE"/>
       <control-input axis="/controls/gear/brake-left" control="BRAKE"/>
  </gear>

  <gear x="2.591" y="-1.825" z="-2.125" 
        compression="0.1" 
        spring="1"
        damp="10"
        dfric="0.9"
        sfric ="0.5">
        <control-input axis="/controls/gear/brake-parking" control="BRAKE"/>
        <control-input axis="/controls/gear/brake-right" control="BRAKE"/>
  </gear>

  <!-- Tail wheel; has castering selectable by a wheel lock -->
  <gear x="-4.842" y="0" z="-0.326" 
        compression="0.05"
        dfric="1.3"
        sfric ="0.9">
        <control-input axis="/controls/gear/tailwheel-lock" src0="0" src1="1" dst0="1" dst1="0" control="CASTERING"/>
  </gear>

  <tank x="1.8" y=" 1.819" z="-0.767" capacity="305"/>
  <tank x="1.8" y="-1.819" z="-0.767" capacity="305"/>

  <ballast x="-3.0" y="0.0" z="0.3" mass="-1100"/>  

</airplane>

