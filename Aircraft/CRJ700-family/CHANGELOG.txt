Changelog:
----------

v1.0.0		final release for FG 2.4.0

v1.0.1		bugfixes
		* Fix "00:60" chronometer/elapsed time bug
		* Fix EICAS trim indicators bug
		* Fix occasional Nasal errors in APU fuel code
		* Fix wrong outline in CRJ700 paintkit

v1.0.2		bugfixes
		* Fix autopilot glitches, including a nonfunctional ILS hold
		* Fix incomplete animated jetway implementation (door positions not broadcasted over MP)
		* Fix livery over MP not working for -ER, -LR, and -EuroLite variants
		* Fix erroneous cruise speeds and weights in the FDM's
		* Fix occasional Nasal errors from ghost fuel tanks
		* Fix the "APU START" message on the EICAS (it always appeared, even when the APU wasn't actually starting)
		* Fix standby compass light switch not working
		* Fix autothrottle being able to apply up to 100% TO/GA power
		* Fix reverse thrust sounds
		* A few documentation revisions

		new features
		* A working implementation of Anders Gidenstam's Dual Control system
		* Improved cockpit texturing
		* A new overspeed alarm and improved master caution/warning alarms
		* Add versioning information to -main.xml

v1.0.3		bugfixes
		* Fix a bug with the Dual Control implementation - the copilot could toggle ANY key command on the pilot's system, possibility for malice...
		* Versioning and author information has been moved to the -set.xml files instead of the -main.xml, so automated processors will read them

		new features
		* Even more improved cockpit texturing
		* New flight management system (FMS) and corresponding control display unit (CDU)
		* New advisory-only (like real life) vertical navigation system (VNAV) with indicators on the PFD; control it via the CDU
		* New antennas, ram air turbine (RAT), and static dischargers added to the model
		* RAT and electrical system added to the 'Failures' dialog

v1.0.4		bugfixes
		* Fix a major bug with the CDU - switching pages would generate cumulative lag
		* Scale the slats along the Z axis a bit to mitigate some of the Z-fighting
		* Ctrl-R reload CDU shortcut removed (it was intended for developers only)
		
v1.1.0		new features
		* Full compatibility with Rembrandt
		* Improved landing gear models
		* New sounds by 07-BUT and SP-LEC
		
		bugfixes
		* Some AP adjustments
		* Major dual control overhaul: control sharing works via dedicated properties irrespective of keyboard shortcuts
