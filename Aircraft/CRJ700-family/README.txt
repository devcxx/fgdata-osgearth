Bombardier CRJ700 series
v1.0.4

----------------------------------

* All files EXCEPT those in the Docs/ folder are licensed under the GNU General Public License version 2, which can be viewed in Docs/License-GPL-2.txt.

* Files in the Docs/ folder are licensed under the GNU Free Documentation License version 1.3, which can be viewed in Docs/License-FDL-1.3.txt.

For aircraft help and credits please see the Docs/ folder. If I forgot to credit you, feel free to raise hell on the mailing list. :-)
