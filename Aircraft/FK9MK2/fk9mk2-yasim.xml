<?xml version="1.0" encoding="UTF-8"?>

<!-- FK 9 Mark 2
     3D model by Emmanuel BARANGER
     FDM      by Emmanuel BARANGER and Maik Justus
        minimum airspeed: 35kts
	cruise: 70kts
	vne=: 97kts
-->

<airplane mass="566">

  <!-- Approach configuration -->
  <approach speed="40" aoa="7" fuel="0.2">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="0.2"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="1.0"/>
    <control-setting axis="/controls/flight/flaps" value="1.0"/>
    <control-setting axis="/sim/weight[0]/weight-lb" value ="180"/>
    <control-setting axis="/sim/weight[1]/weight-lb" value ="180"/>
  </approach>

  <!-- Cruise configuration -->
  <cruise speed="70" alt="6000" fuel="0.5">
    <control-setting axis="/controls/engines/engine[0]/throttle" value="0.5"/>
    <control-setting axis="/controls/engines/engine[0]/mixture" value="0.6"/>
    <control-setting axis="/controls/flight/flaps" value="0.0"/>
    <control-setting axis="/sim/weight[0]/weight-lb" value ="180"/>
    <control-setting axis="/sim/weight[1]/weight-lb" value ="180"/>
  </cruise>

  <cockpit x="1.056" y="0.206" z="0.315"/>

  <fuselage ax="2.922" ay="0" az="-0.037" 
            bx="-2.876" by="0" bz="0.114"
            width="0.992" 
            taper="0.2" 
            midpoint="0.5"/>

  <wing x="1.116" y="0.408" z="0.473" 
        chord="1.27" 
        length="4.92" 
        taper="1" 
        incidence="0"
        sweep="0" 
        dihedral="0" 
        twist="-8" 
        camber="0.1">
        <stall aoa="24" width="6.0" peak="1.5"/>
        <flap0 start="0" end="0.557" lift="1.2" drag="1.25"/>
        <flap1 start="0.558" end="0.95" lift="1.2" drag="1.1"/>

        <control-input axis="/controls/flight/flaps" control="FLAP0"/>
        <control-input axis="/controls/flight/aileron" control="FLAP1" split="true"/>
        <control-input axis="/controls/flight/aileron-trim" control="FLAP1" split="true"/>

        <control-output control="FLAP0" prop="/surface-positions/flap-pos-norm"/>
        <control-output control="FLAP1" side="left" prop="/surface-positions/left-aileron-pos-norm"/>
        <control-output control="FLAP1" side="right" prop="/surface-positions/right-aileron-pos-norm"/>

        <control-speed control="FLAP0" transition-time="2"/>
  </wing>

  <hstab x="-2.438" y="0.0" z="0.098" 
         taper="0.7" 
         incidence="0" 
         length="1.213" 
         chord="0.864" 
         sweep="0">
         <stall aoa="25" width="2" peak="1.5"/>
         <flap0 start="0" end="1" lift="1.5" drag="1.3" effectiveness="1.5"/>

         <control-input axis="/controls/flight/elevator" control="FLAP0"/>
         <control-input axis="/controls/flight/elevator-trim" control="FLAP0"/>

         <control-output control="FLAP0" prop="/surface-positions/elevator-pos-norm"/>
  </hstab>

  <vstab x="-2.40" y="0" z="0.106" 
         taper="0.370" 
         length="0.956" 
         chord="0.992">
         <stall aoa="14" width="3" peak="1.5"/>
         <flap0 start="0" end="1" lift="1.75" drag="1.35"/>

         <control-input axis="/controls/flight/rudder" control="FLAP0" invert="true"/>
         <control-input axis="/controls/flight/rudder-trim" control="FLAP0" invert="true"/>

         <control-output control="FLAP0" prop="/surface-positions/rudder-pos-norm" min="1" max="-1"/>
  </vstab>

  <!-- engine:    ROTAX 912 UL
       propeller: Junkers Profly 3R-Maxi
  =================================================================================
  TO-/MC- engine power(kW): 59,6/58
                      RPM : 5800/5500
  propeller max RPM: 2552
  radius: 1,70 m
  MTOM: 450 kg -->
  <propeller x="2.162" y="0.0" z="-0.137" 
             radius="0.85"
	     cruise-speed="94" 
	     cruise-rpm="2312"
	     cruise-alt="6000" 
	     cruise-power="58"
	     takeoff-power="70" 
	     takeoff-rpm="2100"
	     mass="124" 
	     moment="0.7"  
	     gear-ratio="0.440" >
    <actionpt x="2.636" y="0.0" z="-0.037" />
    <piston-engine eng-rpm="5800" 
                   eng-power="80"
                   displacement="73.91" 
	           compression="9.0"
                   min-throttle="0.05"/>
    <control-input control="THROTTLE" axis="/controls/engines/engine[0]/throttle" src0="0.0" src1="1.0" dst0="0.0" dst1="1.0"/>
    <control-input control="STARTER" axis="/controls/engines/engine[0]/starter" />
    <control-input control="MAGNETOS" axis="/controls/engines/engine[0]/magnetos" />
    <control-input control="MIXTURE" axis="/controls/engines/engine[0]/mixture" src0="0.0" src1="1.0" dst0="0.8" dst1="0.8"/>
  </propeller>
  
  <!-- Left wheel -->
  <gear x="1.054" y="0.850" z="-1.066" 
        compression="0.2" 
        spring="1.8" 
        damp="0.03" 
        on-solid="1" >
        <control-input axis="/controls/gear/brake-left" control="BRAKE" split="true" src0="0.0" src1="1.0" dst0="0.0" dst1="0.4"/>
        <control-input axis="/controls/gear/brake-parking" control="BRAKE" split="true"/>
  </gear>

  <!-- Right wheel -->
  <gear x="1.054" y="-0.850" z="-1.066" 
        compression="0.2" 
        spring="1.8"
        damp="0.03" 
        on-solid="1" >
        <control-input axis="/controls/gear/brake-right" control="BRAKE" split="true" src0="0.0" src1="1.0" dst0="0.0" dst1="0.4"/>
        <control-input axis="/controls/gear/brake-parking" control="BRAKE" split="true"/>
  </gear>

  <!-- Tail wheel -->
  <gear x="-2.849" y="0" z="-0.354" 
        compression="0.15" 
        spring="0.8"
        on-solid="1"
        sfric="1"
        dfric="1">
        <control-input axis="/controls/flight/rudder" control="STEER" src0="-1.0" src1="1.0" dst0="0.5" dst1="-0.5"/>
  </gear>

  <tank x="0.456" y="0" z="-0.310" capacity="56" />

  <!-- pilot -->
  <weight x="0.8" y="0.3" z="-0.1" mass-prop="/sim/weight[0]/weight-lb"/>

  <!-- copilot -->
  <weight x="0.8" y="-0.3" z="-0.1" mass-prop="/sim/weight[1]/weight-lb"/>

  <!-- baggage -->
  <weight x="0.4" y="0" z="0.2" mass-prop="/sim/weight[2]/weight-lb"/>
	
  <!-- Mass distribution -->
  <!-- CG Middle of chord : x=0.825 y=0 z=0.548 -->
  <!--         updated to : x=1.1076 y=0 z=0.548 -->
  <ballast x="1.2" y="0.0" z="1.055" mass="50"/>

</airplane>
